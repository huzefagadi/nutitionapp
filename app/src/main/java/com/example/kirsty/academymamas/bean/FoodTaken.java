package com.example.kirsty.academymamas.bean;

import com.example.kirsty.academymamas.entity.Food;
import com.example.kirsty.academymamas.entity.FoodDiary;

public class FoodTaken {
    public FoodDiary foodDiary;
    public Food food;
}

package com.example.kirsty.academymamas.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

import java.util.Date;

@Entity(nameInDb = "user")
public class User {

    @Id()
    private String id;

    @Property(nameInDb = "email")
    private String email;

    @Property(nameInDb = "name")
    private String name;

    @Property(nameInDb = "weeks")
    private int weeks;

    @Property(nameInDb = "measurement")
    private String measurement;

    @Property(nameInDb = "height")
    private double height;

    @Property(nameInDb = "weight")
    private double weight;

    @Property(nameInDb = "activity")
    private int activity;

    @Property(nameInDb = "week_joined")
    private int weekJoined;

    @Property(nameInDb = "date_created")
    private Date createDate;

    @Generated(hash = 1136372980)
    public User(String id, String email, String name, int weeks, String measurement,
            double height, double weight, int activity, int weekJoined,
            Date createDate) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.weeks = weeks;
        this.measurement = measurement;
        this.height = height;
        this.weight = weight;
        this.activity = activity;
        this.weekJoined = weekJoined;
        this.createDate = createDate;
    }

    @Generated(hash = 586692638)
    public User() {
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeeks() {
        return this.weeks;
    }

    public void setWeeks(int weeks) {
        this.weeks = weeks;
    }

    public String getMeasurement() {
        return this.measurement;
    }

    public void setMeasurement(String measurement) {
        this.measurement = measurement;
    }

    public double getHeight() {
        return this.height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return this.weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getActivity() {
        return this.activity;
    }

    public void setActivity(int activity) {
        this.activity = activity;
    }

    public int getWeekJoined() {
        return this.weekJoined;
    }

    public void setWeekJoined(int weekJoined) {
        this.weekJoined = weekJoined;
    }

    public Date getCreateDate() {
        return this.createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

}

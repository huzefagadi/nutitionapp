package com.example.kirsty.academymamas.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;

@Entity(nameInDb = "food_diary")
public class FoodDiary {

    @Id(autoincrement = true)
    private Long id;

    @Property(nameInDb = "date")
    private Date date;

    @Property(nameInDb = "mealNumber")
    private int mealNumber;

    @Property(nameInDb = "food_id")
    private Long foodId;

    @Property(nameInDb = "fd_serving_size_gram")
    private Double servingSize;

    @Generated(hash = 411253872)
    public FoodDiary(Long id, Date date, int mealNumber, Long foodId,
            Double servingSize) {
        this.id = id;
        this.date = date;
        this.mealNumber = mealNumber;
        this.foodId = foodId;
        this.servingSize = servingSize;
    }

    @Generated(hash = 166822892)
    public FoodDiary() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getMealNumber() {
        return this.mealNumber;
    }

    public void setMealNumber(int mealNumber) {
        this.mealNumber = mealNumber;
    }

    public Long getFoodId() {
        return this.foodId;
    }

    public void setFoodId(Long foodId) {
        this.foodId = foodId;
    }

    public Double getServingSize() {
        return this.servingSize;
    }

    public void setServingSize(Double servingSize) {
        this.servingSize = servingSize;
    }

}

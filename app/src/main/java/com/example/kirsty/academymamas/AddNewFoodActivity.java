package com.example.kirsty.academymamas;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kirsty.academymamas.entity.Food;
import com.example.kirsty.academymamas.entity.FoodDao;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddNewFoodActivity extends AppCompatActivity {


    @BindView(R.id.editTextEditFoodName)
    EditText editTextEditFoodName;
    @BindView(R.id.editTextEditFoodManufactor)
    EditText editTextEditFoodManufactor;
    @BindView(R.id.editTextEditFoodDescription)
    EditText editTextEditFoodDescription;
    @BindView(R.id.editTextEditFoodBarcode)
    EditText editTextEditFoodBarcode;
    @BindView(R.id.editTextEditFoodSize)
    EditText editTextEditFoodSize;
    @BindView(R.id.editTextEditFoodMesurment)
    EditText editTextEditFoodMesurment;
    @BindView(R.id.editTextEditFoodNumber)
    EditText editTextEditFoodNumber;
    @BindView(R.id.editTextEditFoodWord)
    EditText editTextEditFoodWord;
    @BindView(R.id.editTextEditFoodFatPerHundred)
    MaterialEditText editTextEditFoodFatPerHundred;
    @BindView(R.id.editTextEditFoodCarbsPerHundred)
    MaterialEditText editTextEditFoodCarbsPerHundred;
    @BindView(R.id.editTextEditFoodEnergyPerHundred)
    MaterialEditText editTextEditFoodEnergyPerHundred;
    @BindView(R.id.editTextEditFoodProteinsPerHundred)
    MaterialEditText editTextEditFoodProteinsPerHundred;
    @BindView(R.id.editTextEditFoodFolicPerHundred)
    MaterialEditText editTextEditFoodFolicPerHundred;
    @BindView(R.id.editTextEditFoodVitCPerHundred)
    MaterialEditText editTextEditFoodVitCPerHundred;
    @BindView(R.id.editTextEditFoodVitBPerHundred)
    MaterialEditText editTextEditFoodVitBPerHundred;
    @BindView(R.id.editTextEditFoodVitDPerHundred)
    MaterialEditText editTextEditFoodVitDPerHundred;
    @BindView(R.id.editTextEditFoodIronPerHundred)
    MaterialEditText editTextEditFoodIronPerHundred;
    @BindView(R.id.editTextEditFoodZincPerHundred)
    MaterialEditText editTextEditFoodZincPerHundred;
    @BindView(R.id.editTextEditFoodFibrePerHundred)
    MaterialEditText editTextEditFoodFibrePerHundred;


    private boolean isEdit;
    private FoodDao mFoodDao;
    private long mFoodId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_food_edit);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        setTitle("Add food");
        mFoodDao = ((MyApplication) getApplication()).getDaoSession().getFoodDao();
        isEdit = getIntent().getBooleanExtra(Constants.INTENT_IS_EDIT, false);
        if (isEdit) {
            mFoodId = getIntent().getLongExtra(Constants.INTENT_FOOD_ID, 0);
            Food food = mFoodDao.load(mFoodId);
            populateFields(food);
        }
        /* SubmitButton listener */
        Button buttonEditFood = (Button) findViewById(R.id.buttonEditFood);
        buttonEditFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonAddFoodSubmitOnClick();
            }
        });
    }

    private void populateFields(Food food) {
        editTextEditFoodName.setText(food.getName());
        editTextEditFoodManufactor.setText(food.getManufacturer());
        editTextEditFoodDescription.setText(food.getDescription());
        editTextEditFoodBarcode.setText(food.getBarcode());
        editTextEditFoodSize.setText(String.valueOf(food.getSize()));
        editTextEditFoodMesurment.setText(food.getMeasurement());
        editTextEditFoodNumber.setText(String.valueOf(food.getNumber()));
        editTextEditFoodWord.setText(food.getWord());
        editTextEditFoodFatPerHundred.setText(String.valueOf(food.getFatPerHundred()));
        editTextEditFoodCarbsPerHundred.setText(String.valueOf(food.getCarbsPerHundred()));
        editTextEditFoodEnergyPerHundred.setText(String.valueOf(food.getEnergyPerHundred()));
        editTextEditFoodProteinsPerHundred.setText(String.valueOf(food.getProteinsPerHundred()));
        editTextEditFoodFolicPerHundred.setText(String.valueOf(food.getFolicPerHundred()));
        editTextEditFoodVitCPerHundred.setText(String.valueOf(food.getVitCPerHundred()));
        editTextEditFoodVitBPerHundred.setText(String.valueOf(food.getVitBPerHundred()));
        editTextEditFoodVitDPerHundred.setText(String.valueOf(food.getVitDPerHundred()));
        editTextEditFoodIronPerHundred.setText(String.valueOf(food.getIronPerHundred()));
        editTextEditFoodZincPerHundred.setText(String.valueOf(food.getZincPerHundred()));
        editTextEditFoodFibrePerHundred.setText(String.valueOf(food.getFibrePerHundred()));
    }

    /*- Button add food submit on click ----------------------------------------------------- */
    public void buttonAddFoodSubmitOnClick() {

        Food food = new Food();
        // Error?
        int error = 0;


        String stringName = editTextEditFoodName.getText().toString();
        if (stringName.isEmpty()) {
            Toast.makeText(this, "Please fill in a name.", Toast.LENGTH_SHORT).show();
            error = 1;
        }

        String stringManufactor = editTextEditFoodManufactor.getText().toString();
        if (stringManufactor.isEmpty()) {
            Toast.makeText(this, "Please fill in a manufactor.", Toast.LENGTH_SHORT).show();
            error = 1;
        }

        // Description
        String stringDescription = editTextEditFoodDescription.getText().toString();

        // Barcode
        String stringBarcode = editTextEditFoodBarcode.getText().toString();

        /* Serving Table */

        // Size
        String stringSize = editTextEditFoodSize.getText().toString();
        double doubleServingSize = 0;
        if (stringSize.isEmpty()) {
            Toast.makeText(this, "Please fill in a size.", Toast.LENGTH_SHORT).show();
            error = 1;
        } else {
            try {
                doubleServingSize = Double.parseDouble(stringSize);

            } catch (NumberFormatException nfe) {
                Toast.makeText(this, "Serving size is not number.", Toast.LENGTH_SHORT).show();
                error = 1;
            }
        }

        // Mesurment
        String stringMesurment = editTextEditFoodMesurment.getText().toString();
        if (stringMesurment.isEmpty()) {
            Toast.makeText(this, "Please fill in mesurment.", Toast.LENGTH_SHORT).show();
            error = 1;
        }

        // Number
        String stringNumber = editTextEditFoodNumber.getText().toString();
        if (stringNumber.isEmpty()) {
            Toast.makeText(this, "Please fill in number.", Toast.LENGTH_SHORT).show();
            error = 1;
        }

        // Word

        String stringWord = editTextEditFoodWord.getText().toString();
        if (stringWord.equals("")) {
            Toast.makeText(this, "Please fill in word.", Toast.LENGTH_SHORT).show();
            error = 1;
        }


        /* Calories table */
        // Energy

        String stringEnergyPerHundred = editTextEditFoodEnergyPerHundred.getText().toString();
        stringEnergyPerHundred = stringEnergyPerHundred.replace(",", ".");
        double doubleEnergyPerHundred = 0;
        if (stringEnergyPerHundred.equals("")) {
            Toast.makeText(this, "Please fill in energy.", Toast.LENGTH_SHORT).show();
            error = 1;
        } else {
            try {
                doubleEnergyPerHundred = Double.parseDouble(stringEnergyPerHundred);

            } catch (NumberFormatException nfe) {
                Toast.makeText(this, "Energy is not a number.", Toast.LENGTH_SHORT).show();
                error = 1;
            }
        }

        // Proteins
        String stringProteinsPerHundred = editTextEditFoodProteinsPerHundred.getText().toString();
        stringProteinsPerHundred = stringProteinsPerHundred.replace(",", ".");
        double doubleProteinsPerHundred = 0;
        if (stringProteinsPerHundred.equals("")) {
            Toast.makeText(this, "Please fill in proteins.", Toast.LENGTH_SHORT).show();
            error = 1;
        } else {
            try {
                doubleProteinsPerHundred = Double.parseDouble(stringProteinsPerHundred);

            } catch (NumberFormatException nfe) {
                Toast.makeText(this, "Protein is not a number.\n" + "You wrote: " + stringProteinsPerHundred, Toast.LENGTH_SHORT).show();
                error = 1;
            }
        }

        // Carbs
        String stringCarbsPerHundred = editTextEditFoodCarbsPerHundred.getText().toString();
        stringCarbsPerHundred = stringCarbsPerHundred.replace(",", ".");
        double doubleCarbsPerHundred = 0;
        if (stringCarbsPerHundred.equals("")) {
            Toast.makeText(this, "Please fill in carbs.", Toast.LENGTH_SHORT).show();
            error = 1;
        } else {
            try {
                doubleCarbsPerHundred = Double.parseDouble(stringCarbsPerHundred);

            } catch (NumberFormatException nfe) {
                Toast.makeText(this, "Carbs is not a number.\nYou wrote: " + stringCarbsPerHundred, Toast.LENGTH_SHORT).show();
                error = 1;
            }
        }

        // Fat
        String stringFatPerHundred = editTextEditFoodFatPerHundred.getText().toString();
        stringFatPerHundred = stringFatPerHundred.replace(",", ".");
        double doubleFatPerHundred = 0;
        if (stringFatPerHundred.equals("")) {
            Toast.makeText(this, "Please fill in fat.", Toast.LENGTH_SHORT).show();
            error = 1;
        } else {
            try {
                doubleFatPerHundred = Double.parseDouble(stringFatPerHundred);

            } catch (NumberFormatException nfe) {
                Toast.makeText(this, "Carbs is not a number.", Toast.LENGTH_SHORT).show();
                error = 1;
            }
        }


        // folic
        String stringFolicPerHundred = editTextEditFoodFolicPerHundred.getText().toString();
        double doubleFolicPerHundred = 0;
        if (stringFolicPerHundred.isEmpty()) {
            Toast.makeText(this, "Please fill in folic.", Toast.LENGTH_SHORT).show();
            error = 1;
        } else {
            try {
                doubleFolicPerHundred = Double.parseDouble(stringFolicPerHundred);

            } catch (NumberFormatException nfe) {
                Toast.makeText(this, "folic is not a number.", Toast.LENGTH_SHORT).show();
                error = 1;
            }
        }


        // Vit C
        String stringVitCPerHundred = editTextEditFoodVitCPerHundred.getText().toString();
        double doubleVitCPerHundred = 0;
        if (stringVitCPerHundred.isEmpty()) {
            Toast.makeText(this, "Please fill in Vit C.", Toast.LENGTH_SHORT).show();
            error = 1;
        } else {
            try {
                doubleVitCPerHundred = Double.parseDouble(stringVitCPerHundred);

            } catch (NumberFormatException nfe) {
                Toast.makeText(this, "Vit C is not a number.", Toast.LENGTH_SHORT).show();
                error = 1;
            }
        }


        // Vic B
        String stringVitBPerHundred = editTextEditFoodVitBPerHundred.getText().toString();
        double doubleVitBPerHundred = 0;
        if (stringVitBPerHundred.isEmpty()) {
            Toast.makeText(this, "Please fill in Vit B.", Toast.LENGTH_SHORT).show();
            error = 1;
        } else {
            try {
                doubleVitBPerHundred = Double.parseDouble(stringVitBPerHundred);

            } catch (NumberFormatException nfe) {
                Toast.makeText(this, "Vit B is not a number.", Toast.LENGTH_SHORT).show();
                error = 1;
            }
        }

        // Vic D
        String stringVitDPerHundred = editTextEditFoodVitDPerHundred.getText().toString();
        double doubleVitDPerHundred = 0;
        if (stringVitDPerHundred.isEmpty()) {
            Toast.makeText(this, "Please fill in Vit D.", Toast.LENGTH_SHORT).show();
            error = 1;
        } else {
            try {
                doubleVitDPerHundred = Double.parseDouble(stringVitDPerHundred);

            } catch (NumberFormatException nfe) {
                Toast.makeText(this, "Vit D is not a number.", Toast.LENGTH_SHORT).show();
                error = 1;
            }
        }


        // Iron
        String stringIronPerHundred = editTextEditFoodIronPerHundred.getText().toString();
        double doubleIronPerHundred = 0;
        if (stringIronPerHundred.isEmpty()) {
            Toast.makeText(this, "Please fill in Iron.", Toast.LENGTH_SHORT).show();
            error = 1;
        } else {
            try {
                doubleIronPerHundred = Double.parseDouble(stringIronPerHundred);

            } catch (NumberFormatException nfe) {
                Toast.makeText(this, "Iron is not a number.", Toast.LENGTH_SHORT).show();
                error = 1;
            }
        }


        // Zinc
        String stringZincPerHundred = editTextEditFoodZincPerHundred.getText().toString();
        double doubleZincPerHundred = 0;
        if (stringZincPerHundred.isEmpty()) {
            Toast.makeText(this, "Please fill in Zinc.", Toast.LENGTH_SHORT).show();
            error = 1;
        } else {
            try {
                doubleZincPerHundred = Double.parseDouble(stringZincPerHundred);

            } catch (NumberFormatException nfe) {
                Toast.makeText(this, "Zinc is not a number.", Toast.LENGTH_SHORT).show();
                error = 1;
            }
        }

        // Fibre
        String stringFibrePerHundred = editTextEditFoodFibrePerHundred.getText().toString();
        double doubleFibrePerHundred = 0;
        if (stringFibrePerHundred.isEmpty()) {
            Toast.makeText(this, "Please fill in Fibre.", Toast.LENGTH_SHORT).show();
            error = 1;
        } else {
            try {
                doubleFibrePerHundred = Double.parseDouble(stringFibrePerHundred);

            } catch (NumberFormatException nfe) {
                Toast.makeText(this, "Fibre is not a number.", Toast.LENGTH_SHORT).show();
                error = 1;
            }
        }



        /* Insert */
        if (error == 0) {

            /* Calories table pr meal */
            double energyCalculated = Math.round((doubleEnergyPerHundred * doubleServingSize) / 100);
            double proteinsCalculated = Math.round((doubleProteinsPerHundred * doubleServingSize) / 100);
            double carbsCalculated = Math.round((doubleCarbsPerHundred * doubleServingSize) / 100);
            double fatCalculated = Math.round((doubleFatPerHundred * doubleServingSize) / 100);
            food.setName(stringName);
            food.setManufacturer(stringManufactor);
            food.setDescription(stringDescription);
            food.setBarcode(stringBarcode);
            food.setSize(doubleServingSize);
            food.setMeasurement(stringMesurment);
            food.setNumber(Double.parseDouble(stringNumber));
            food.setWord(stringWord);
            food.setEnergyPerHundred(doubleEnergyPerHundred);
            food.setProteinsPerHundred(doubleProteinsPerHundred);
            food.setCarbsPerHundred(doubleCarbsPerHundred);
            food.setFatPerHundred(doubleFatPerHundred);
            food.setFolicPerHundred(doubleFolicPerHundred);
            food.setVitBPerHundred(doubleVitBPerHundred);
            food.setVitCPerHundred(doubleVitCPerHundred);
            food.setVitDPerHundred(doubleVitDPerHundred);
            food.setZincPerHundred(doubleZincPerHundred);
            food.setIronPerHundred(doubleIronPerHundred);
            food.setFibrePerHundred(doubleFibrePerHundred);
            food.setEnergyCalculated(energyCalculated);
            food.setProteinsCalculated(proteinsCalculated);
            food.setCarbsCalculated(carbsCalculated);
            food.setFatCalculated(fatCalculated);

//
//            String fields =
//                    "_id, " +
//                            "food_name, " +
//                            "food_manufactor_name, " +
//                            "food_description, " +
//                            "food_serving_size_gram, " +
//                            "food_serving_size_gram_mesurment, " +
//                            "food_serving_size_pcs, " +
//                            "food_serving_size_pcs_mesurment, " +
//                            "food_energy, " +
//                            "food_proteins, " +
//                            "food_carbohydrates, " +
//                            "food_fat, " +
//                            "food_energy_calculated, " +
//                            "food_proteins_calculated, " +
//                            "food_carbohydrates_calculated, " +
//                            "food_fat_calculated, " +
//                            "food_barcode, " +
//                            "food_category_id";
//
//            String values =
//                    "NULL, " +
//                            stringNameSQL + ", " +
//                            stringManufactorSQL + ", " +
//                            stringDescriptionSQL + ", " +
//                            stringSizeSQL + ", " +
//                            stringMesurmentSQL + ", " +
//                            stringNumberSQL + ", " +
//                            stringWordSQL + ", " +
//                            stringEnergyPerHundredSQL + ", " +
//                            stringProteinsPerHundredSQL + ", " +
//                            stringCarbsPerHundredSQL + ", " +
//                            stringFatPerHundredSQL + ", " +
//                            stringEnergyCalculatedSQL + ", " +
//                            stringProteinsCalculatedSQL + ", " +
//                            stringCarbsCalculatedSQL + ", " +
//                            stringfatCalculatedSQL + ", " +
//                            stringBarcodeSQL;


            //   db.insert("food", fields, values);


            if (isEdit) {
                food.setId(mFoodId);
                mFoodDao.update(food);
                Toast.makeText(this, "Food updated", Toast.LENGTH_SHORT).show();
            } else {

                mFoodDao.insert(food);
                Toast.makeText(this, "Food created", Toast.LENGTH_SHORT).show();
            }


            // Toast

            finish();

//            // Move user back to correct design
//            FragmentManager fragmentManager = this.getSupportFragmentManager();
//            fragmentManager.beginTransaction().replace(R.id.flContent, new FoodFragment(), FoodFragment.class.getName()).commit();

        } // error == 0


        /* Close db */
    } // buttonAddFoodSubmitOnClick

}

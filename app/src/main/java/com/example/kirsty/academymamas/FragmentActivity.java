package com.example.kirsty.academymamas;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.kirsty.academymamas.fragments.BabyNeedsFragment;
import com.example.kirsty.academymamas.fragments.ChangePasswordFragment;
import com.example.kirsty.academymamas.fragments.DashboardFragment;
import com.example.kirsty.academymamas.fragments.DonateFragment;
import com.example.kirsty.academymamas.fragments.EditProfileFragment;
import com.example.kirsty.academymamas.fragments.FoodDiaryFragment;
import com.example.kirsty.academymamas.fragments.FoodFragment;
import com.example.kirsty.academymamas.fragments.ForumFragment;
import com.example.kirsty.academymamas.fragments.MealIdeasFragment;
import com.example.kirsty.academymamas.fragments.SignOutFragment;
import com.example.kirsty.academymamas.interfaces.OnFragmentInteractionListener;

public class FragmentActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        OnFragmentInteractionListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        /* Toolbar */
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /* Inialize fragmet */
        Fragment fragment = null;
        Class fragmentClass = null;
        fragmentClass = DashboardFragment.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        try {
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        } catch (Exception e) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();

        }


        /* Navigation */
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        // Navigation items
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Fragment fragment = null;
        Class fragmentClass = null;

        if (id == R.id.nav_dashboard) {
            fragmentClass = DashboardFragment.class;
        } else if (id == R.id.nav_foodDiary) {
            fragmentClass = FoodDiaryFragment.class;
        } else if (id == R.id.nav_mealIdeas) {
            fragmentClass = MealIdeasFragment.class;
        } else if (id == R.id.nav_forum) {
            fragmentClass = ForumFragment.class;
        } else if (id == R.id.nav_babyNeeds) {
            fragmentClass = BabyNeedsFragment.class;
        } else if (id == R.id.nav_food) {
            fragmentClass = FoodFragment.class;
        } else if (id == R.id.nav_Donate) {
            fragmentClass = DonateFragment.class;
        } else if (id == R.id.nav_edit_profile) {
            fragmentClass = EditProfileFragment.class;
        } else if (id == R.id.nav_change_password) {
            fragmentClass = ChangePasswordFragment.class;
        } else if (id == R.id.nav_sign_out) {
            fragmentClass = SignOutFragment.class;
        }

        // Try to add item to fragment
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Try to show that content
        FragmentManager fragmentManager = getSupportFragmentManager();
        try {
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Error: " + e.toString(), Toast.LENGTH_LONG).show();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void pushFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.flContent, fragment).commit();
    }

    @Override
    public void onFragmentBackPressed(Uri uri) {

    }

    @Override
    public void showActivity(Intent intent) {
        startActivity(intent);
    }
}

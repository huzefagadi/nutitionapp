package com.example.kirsty.academymamas.interfaces;

public interface AdapterCallback {

    void onItemClicked(int position);
}

package com.example.kirsty.academymamas.entity;

import android.database.SQLException;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;

@Entity(nameInDb = "food_diary_sum")
public class FoodDiarySum {

    @Id(autoincrement = true)
    private Long id;

    @Property(nameInDb = "fdce_date")
    private Date date;

    @Property(nameInDb = "fdce_eaten_energy")
    private int energy;

    @Property(nameInDb = "fdce_eaten_proteins")
    private int proteins;

    @Property(nameInDb = "fdce_eaten_carbs")
    private int carbohydrates;

    @Property(nameInDb = "fdce_eaten_fat")
    private int fat;

    @Generated(hash = 1564290569)
    public FoodDiarySum(Long id, Date date, int energy, int proteins,
            int carbohydrates, int fat) {
        this.id = id;
        this.date = date;
        this.energy = energy;
        this.proteins = proteins;
        this.carbohydrates = carbohydrates;
        this.fat = fat;
    }

    @Generated(hash = 1098504216)
    public FoodDiarySum() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getEnergy() {
        return this.energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public int getProteins() {
        return this.proteins;
    }

    public void setProteins(int proteins) {
        this.proteins = proteins;
    }

    public int getCarbohydrates() {
        return this.carbohydrates;
    }

    public void setCarbohydrates(int carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public int getFat() {
        return this.fat;
    }

    public void setFat(int fat) {
        this.fat = fat;
    }

}


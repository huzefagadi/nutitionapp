package com.example.kirsty.academymamas.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kirsty.academymamas.entity.Food;
import com.example.kirsty.academymamas.R;
import com.example.kirsty.academymamas.interfaces.AdapterCallback;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FoodRecyclerViewAdapter extends RecyclerView.Adapter<FoodRecyclerViewAdapter.ViewHolder> {

    private List<Food> mData;
    private LayoutInflater mInflater;
    private AdapterCallback adapterCallback;

    // data is passed into the constructor
    public FoodRecyclerViewAdapter(Context context, List<Food> data, AdapterCallback adapterCallback) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.adapterCallback = adapterCallback;
    }

    public List<Food> getData() {
        return mData;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.food_recycler_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Food food = mData.get(position);
        holder.mNameTv.setText(food.getName());

        String subLine = food.getManufacturer() + ", " +
                food.getSize() + " " +
                food.getMeasurement() + ", " +
                food.getNumber() + " " +
                food.getWord();
        holder.mSubNameTv.setText(subLine);
        holder.mNumberTv.setText(String.valueOf(food.getEnergyCalculated()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapterCallback.onItemClicked(position);
            }
        });
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewListName)
        TextView mNameTv;
        @BindView(R.id.textViewListNumber)
        TextView mNumberTv;
        @BindView(R.id.textViewListSubName)
        TextView mSubNameTv;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

package com.example.kirsty.academymamas.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

import java.io.Serializable;

@Entity(nameInDb = "food")
public class Food {

    @Id(autoincrement = true)
    private Long id;

    @Property(nameInDb = "name")
    private String name;

    @Property(nameInDb = "manufacturer")
    private String manufacturer;

    @Property(nameInDb = "description")
    private String description;

    @Property(nameInDb = "size")
    private Double size;

    @Property(nameInDb = "measurement")
    private String measurement;

    @Property(nameInDb = "number")
    private Double number;

    @Property(nameInDb = "word")
    private String word;

    @Property(nameInDb = "energyPerHundred")
    private Double energyPerHundred;
    @Property(nameInDb = "proteinsPerHundred")
    private Double proteinsPerHundred;
    @Property(nameInDb = "carbsPerHundred")
    private Double carbsPerHundred;
    @Property(nameInDb = "fatPerHundred")
    private Double fatPerHundred;
    @Property(nameInDb = "folicAcidPerHundered")
    private Double folicPerHundred;
    @Property(nameInDb = "vitCPerHundred")
    private Double vitCPerHundred;
    @Property(nameInDb = "vitBPerHundred")
    private Double vitBPerHundred;
    @Property(nameInDb = "vitDPerHundred")
    private Double vitDPerHundred;
    @Property(nameInDb = "zincPerHundred")
    private Double zincPerHundred;
    @Property(nameInDb = "fibrePerHundred")
    private Double fibrePerHundred;
    @Property(nameInDb = "ironPerHundred")
    private Double ironPerHundred;

    @Property(nameInDb = "energyCalculated")
    private Double energyCalculated;
    @Property(nameInDb = "proteinsCalculated")
    private Double proteinsCalculated;
    @Property(nameInDb = "carbsCalculated")
    private Double carbsCalculated;
    @Property(nameInDb = "fatCalculated")
    private Double fatCalculated;
    @Property(nameInDb = "barcode")
    private String barcode;
    @Property(nameInDb = "categoryId")
    private String categoryId;
    @Generated(hash = 814670302)
    public Food(Long id, String name, String manufacturer, String description,
            Double size, String measurement, Double number, String word,
            Double energyPerHundred, Double proteinsPerHundred,
            Double carbsPerHundred, Double fatPerHundred, Double folicPerHundred,
            Double vitCPerHundred, Double vitBPerHundred, Double vitDPerHundred,
            Double zincPerHundred, Double fibrePerHundred, Double ironPerHundred,
            Double energyCalculated, Double proteinsCalculated,
            Double carbsCalculated, Double fatCalculated, String barcode,
            String categoryId) {
        this.id = id;
        this.name = name;
        this.manufacturer = manufacturer;
        this.description = description;
        this.size = size;
        this.measurement = measurement;
        this.number = number;
        this.word = word;
        this.energyPerHundred = energyPerHundred;
        this.proteinsPerHundred = proteinsPerHundred;
        this.carbsPerHundred = carbsPerHundred;
        this.fatPerHundred = fatPerHundred;
        this.folicPerHundred = folicPerHundred;
        this.vitCPerHundred = vitCPerHundred;
        this.vitBPerHundred = vitBPerHundred;
        this.vitDPerHundred = vitDPerHundred;
        this.zincPerHundred = zincPerHundred;
        this.fibrePerHundred = fibrePerHundred;
        this.ironPerHundred = ironPerHundred;
        this.energyCalculated = energyCalculated;
        this.proteinsCalculated = proteinsCalculated;
        this.carbsCalculated = carbsCalculated;
        this.fatCalculated = fatCalculated;
        this.barcode = barcode;
        this.categoryId = categoryId;
    }
    @Generated(hash = 866324199)
    public Food() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getManufacturer() {
        return this.manufacturer;
    }
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
    public String getDescription() {
        return this.description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Double getSize() {
        return this.size;
    }
    public void setSize(Double size) {
        this.size = size;
    }
    public String getMeasurement() {
        return this.measurement;
    }
    public void setMeasurement(String measurement) {
        this.measurement = measurement;
    }
    public Double getNumber() {
        return this.number;
    }
    public void setNumber(Double number) {
        this.number = number;
    }
    public String getWord() {
        return this.word;
    }
    public void setWord(String word) {
        this.word = word;
    }
    public Double getEnergyPerHundred() {
        return this.energyPerHundred;
    }
    public void setEnergyPerHundred(Double energyPerHundred) {
        this.energyPerHundred = energyPerHundred;
    }
    public Double getProteinsPerHundred() {
        return this.proteinsPerHundred;
    }
    public void setProteinsPerHundred(Double proteinsPerHundred) {
        this.proteinsPerHundred = proteinsPerHundred;
    }
    public Double getCarbsPerHundred() {
        return this.carbsPerHundred;
    }
    public void setCarbsPerHundred(Double carbsPerHundred) {
        this.carbsPerHundred = carbsPerHundred;
    }
    public Double getFatPerHundred() {
        return this.fatPerHundred;
    }
    public void setFatPerHundred(Double fatPerHundred) {
        this.fatPerHundred = fatPerHundred;
    }
    public Double getEnergyCalculated() {
        return this.energyCalculated;
    }
    public void setEnergyCalculated(Double energyCalculated) {
        this.energyCalculated = energyCalculated;
    }
    public Double getProteinsCalculated() {
        return this.proteinsCalculated;
    }
    public void setProteinsCalculated(Double proteinsCalculated) {
        this.proteinsCalculated = proteinsCalculated;
    }
    public Double getCarbsCalculated() {
        return this.carbsCalculated;
    }
    public void setCarbsCalculated(Double carbsCalculated) {
        this.carbsCalculated = carbsCalculated;
    }
    public Double getFatCalculated() {
        return this.fatCalculated;
    }
    public void setFatCalculated(Double fatCalculated) {
        this.fatCalculated = fatCalculated;
    }
    public String getBarcode() {
        return this.barcode;
    }
    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
    public String getCategoryId() {
        return this.categoryId;
    }
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }
    public Double getFolicPerHundred() {
        return this.folicPerHundred;
    }
    public void setFolicPerHundred(Double folicPerHundred) {
        this.folicPerHundred = folicPerHundred;
    }
    public Double getVitCPerHundred() {
        return this.vitCPerHundred;
    }
    public void setVitCPerHundred(Double vitCPerHundred) {
        this.vitCPerHundred = vitCPerHundred;
    }
    public Double getVitBPerHundred() {
        return this.vitBPerHundred;
    }
    public void setVitBPerHundred(Double vitBPerHundred) {
        this.vitBPerHundred = vitBPerHundred;
    }
    public Double getVitDPerHundred() {
        return this.vitDPerHundred;
    }
    public void setVitDPerHundred(Double vitDPerHundred) {
        this.vitDPerHundred = vitDPerHundred;
    }
    public Double getZincPerHundred() {
        return this.zincPerHundred;
    }
    public void setZincPerHundred(Double zincPerHundred) {
        this.zincPerHundred = zincPerHundred;
    }
    public Double getFibrePerHundred() {
        return this.fibrePerHundred;
    }
    public void setFibrePerHundred(Double fibrePerHundred) {
        this.fibrePerHundred = fibrePerHundred;
    }
    public Double getIronPerHundred() {
        return this.ironPerHundred;
    }
    public void setIronPerHundred(Double ironPerHundred) {
        this.ironPerHundred = ironPerHundred;
    }


}

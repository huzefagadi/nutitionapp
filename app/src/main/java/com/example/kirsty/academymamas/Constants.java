package com.example.kirsty.academymamas;

public class Constants {
    public static final String INTENT_FOOD_ID = "foodId";
    public static final String INTENT_IS_EDIT = "isEdit";
    public static final String INTENT_MEAL_NUMBER = "mealNumber";
    public static final String INTENT_ACTION = "action";
    public static final String ADD_MEAL_FOR_DAY = "addMeal";
}

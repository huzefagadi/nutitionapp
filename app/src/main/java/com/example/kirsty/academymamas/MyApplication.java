package com.example.kirsty.academymamas;

import android.app.Application;

import com.example.kirsty.academymamas.database.DbOpenHelper;
import com.example.kirsty.academymamas.entity.DaoMaster;
import com.example.kirsty.academymamas.entity.DaoSession;

public class MyApplication extends Application {

    private DaoSession mDaoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        mDaoSession = new DaoMaster(
                new DbOpenHelper(this, "databases.db").getWritableDb()).newSession();

        // USER CREATION FOR DEMO PURPOSE
        if (mDaoSession.getFoodDao().loadAll().size() == 0) {
            // mDaoSession.getUserDao().insert(new User(1L, "Janishar Ali","", ""));
        }
    }

    public DaoSession getDaoSession() {
        return mDaoSession;
    }
}


package com.example.kirsty.academymamas;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.kirsty.academymamas.entity.Food;
import com.example.kirsty.academymamas.entity.FoodDao;
import com.example.kirsty.academymamas.entity.FoodDiary;
import com.example.kirsty.academymamas.entity.FoodDiaryDao;

import org.greenrobot.greendao.query.DeleteQuery;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShowFoodDetailsActivity extends AppCompatActivity {

    private Food mCurrentFood;
    @BindView(R.id.textViewViewFoodName)
    TextView textViewViewFoodName;
    @BindView(R.id.textViewViewFoodManufactorName)
    TextView textViewViewFoodManufactorName;
    @BindView(R.id.textViewViewFoodAbout)
    TextView textViewViewFoodAbout;
    @BindView(R.id.textViewViewFoodDescription)
    TextView textViewViewFoodDescription;

    // Calories table
    @BindView(R.id.textViewViewFoodEnergyPerHundred)
    TextView textViewViewFoodEnergyPerHundred;
    @BindView(R.id.textViewViewFoodProteinsPerHundred)
    TextView textViewViewFoodProteinsPerHundred;
    @BindView(R.id.textViewViewFoodCarbsPerHundred)
    TextView textViewViewFoodCarbsPerHundred;
    @BindView(R.id.textViewViewFoodFatPerHundred)
    TextView textViewViewFoodFatPerHundred;
    @BindView(R.id.textViewViewFoodEnergyPerN)
    TextView textViewViewFoodEnergyPerN;
    @BindView(R.id.textViewViewFoodProteinsPerN)
    TextView textViewViewFoodProteinsPerN;
    @BindView(R.id.textViewViewFoodCarbsPerN)
    TextView textViewViewFoodCarbsPerN;
    @BindView(R.id.textViewViewFoodFatPerN)
    TextView textViewViewFoodFatPerN;

    @BindView(R.id.quantity)
    EditText mQuantityEt;
    @BindView(R.id.actionLayout)
    LinearLayout mActionLayout;

    FoodDao mFoodDao;
    FoodDiaryDao mFoodDiaryDao;
    String mAction;
    int mealNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_food_details);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mFoodDao = ((MyApplication) getApplication()).getDaoSession().getFoodDao();
        mFoodDiaryDao = ((MyApplication) getApplication()).getDaoSession().getFoodDiaryDao();
    }

    @Override
    protected void onResume() {
        super.onResume();
        long foodId = getIntent().getLongExtra(Constants.INTENT_FOOD_ID, 0);
        mAction = getIntent().getStringExtra(Constants.INTENT_ACTION);
        try {
            mealNumber = Integer.parseInt(getIntent().getStringExtra(Constants.INTENT_MEAL_NUMBER));
        } catch (Exception e) {
            e.printStackTrace();
        }

        mCurrentFood = mFoodDao.load(foodId);
        loadViewsWithData();
    }

    private void loadViewsWithData() {
        // Headline
        textViewViewFoodName.setText(mCurrentFood.getName());
        textViewViewFoodManufactorName.setText(mCurrentFood.getManufacturer());
        String foodAbout = mCurrentFood.getSize() + " " + mCurrentFood.getMeasurement() + " = " +
                mCurrentFood.getNumber() + " " + mCurrentFood.getWord() + ".";
        textViewViewFoodAbout.setText(foodAbout);

        // Description
        textViewViewFoodDescription.setText(mCurrentFood.getDescription());

        // Calories table
        textViewViewFoodEnergyPerHundred.setText(String.valueOf(mCurrentFood.getEnergyPerHundred()));
        textViewViewFoodProteinsPerHundred.setText(String.valueOf(mCurrentFood.getProteinsPerHundred()));
        textViewViewFoodCarbsPerHundred.setText(String.valueOf(mCurrentFood.getCarbsPerHundred()));
        textViewViewFoodFatPerHundred.setText(String.valueOf(mCurrentFood.getFatPerHundred()));

        textViewViewFoodEnergyPerN.setText(String.valueOf(mCurrentFood.getEnergyCalculated()));
        textViewViewFoodProteinsPerN.setText(String.valueOf(mCurrentFood.getProteinsCalculated()));
        textViewViewFoodCarbsPerN.setText(String.valueOf(mCurrentFood.getCarbsCalculated()));
        textViewViewFoodFatPerN.setText(String.valueOf(mCurrentFood.getFatCalculated()));

        if (Constants.ADD_MEAL_FOR_DAY.equals(mAction)) {
            mActionLayout.setVisibility(View.VISIBLE);
        } else {
            mActionLayout.setVisibility(View.GONE);
        }
    }

    @OnClick({R.id.imageViewAddToDiary, R.id.cancel, R.id.done})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.done:
                verifyAndSaveFoodToDiary();
                break;
            case R.id.cancel:
                finish();
        }
    }

    private void verifyAndSaveFoodToDiary() {
        Double quantity = 0.0;
        try {
            quantity = Double.parseDouble(mQuantityEt.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (quantity == 0.0) {
            Toast.makeText(this, "Please enter proper quanity", Toast.LENGTH_LONG).show();
        } else {
            FoodDiary foodDiary = new FoodDiary();
            foodDiary.setDate(new Date());
            foodDiary.setServingSize(quantity);
            foodDiary.setFoodId(mCurrentFood.getId());
            foodDiary.setMealNumber(mealNumber);
            mFoodDiaryDao.insert(foodDiary);
            Toast.makeText(this, "Food added for the day", Toast.LENGTH_LONG).show();
            finish();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_food_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_action_food_edit:
                editFood();
                return true;
            case R.id.menu_action_food_delete:
                confirmForDelete();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void editFood() {
        Intent intent = new Intent(this, AddNewFoodActivity.class);
        intent.putExtra(Constants.INTENT_IS_EDIT, true);
        intent.putExtra(Constants.INTENT_FOOD_ID, mCurrentFood.getId());
        startActivity(intent);
    }

    private void confirmForDelete() {
        new MaterialDialog.Builder(this)
                .content(R.string.confirm_delete)
                .positiveText(R.string.yes)
                .negativeText(R.string.cancel)
                .onPositive((dialog, which) -> {
                    deleteFood();
                })
                .onNegative((dialog, which) -> dialog.dismiss())
                .show();
    }

    private void deleteFood() {
        mFoodDao.deleteByKey(mCurrentFood.getId());
        final DeleteQuery<FoodDiary> tableDeleteQuery = mFoodDiaryDao.queryBuilder()
                .where(FoodDiaryDao.Properties.FoodId.eq(mCurrentFood.getId()))
                .buildDelete();
        tableDeleteQuery.executeDeleteWithoutDetachingEntities();
        ((MyApplication) getApplication()).getDaoSession().clear();
        Toast.makeText(this, "Food Deleted Successfully", Toast.LENGTH_LONG).show();
        finish();
    }


}

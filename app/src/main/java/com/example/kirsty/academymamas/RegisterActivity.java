package com.example.kirsty.academymamas;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kirsty.academymamas.entity.User;
import com.example.kirsty.academymamas.entity.UserDao;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class RegisterActivity extends AppCompatActivity {

    private TextView useremail;
    private FirebaseAuth auth;
    /* Variables */
    private String[] arraySpinnerDOBDay = new String[31];
    private String[] arraySpinnerDOBYear = new String[100];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        auth = FirebaseAuth.getInstance();
        useremail = (TextView) findViewById(R.id.useremail);


        //get current user
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        useremail.setText(user.getEmail());


        TextView textViewErrorMessage = (TextView) findViewById(R.id.textViewErrorMessage);
        textViewErrorMessage.setVisibility(View.GONE);

        /* Hide inches field */
        EditText editTextHeightInches = (EditText) findViewById(R.id.editTextHeightInches);
        editTextHeightInches.setVisibility(View.GONE);


        /* Listener Mesurment spinner */
        Spinner spinnerMesurment = (Spinner) findViewById(R.id.spinnerMesurment);
        spinnerMesurment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                mesurmentChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // mesurmentChanged();
            }
        });





        /* Listener buttonSignUp */
        Button buttonSignUp = (Button) findViewById(R.id.sign_in_button);
        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpSubmit();
            }
        });


    } // protected void onCreate

    /*- Mesurment changed ------------------------------------------ */
    public void mesurmentChanged() {

        // Mesurment spinner
        Spinner spinnerMesurment = (Spinner) findViewById(R.id.spinnerMesurment);
        String stringMesurment = spinnerMesurment.getSelectedItem().toString();


        EditText editTextHeightCm = (EditText) findViewById(R.id.editTextHeightCm);
        EditText editTextHeightInches = (EditText) findViewById(R.id.editTextHeightInches);
        String stringHeightCm = editTextHeightCm.getText().toString();
        String stringHeightInches = editTextHeightInches.getText().toString();

        double heightCm = 0;
        double heightFeet = 0;
        double heightInches = 0;

        TextView textViewCm = (TextView) findViewById(R.id.textViewCm);
        TextView textViewKg = (TextView) findViewById(R.id.textViewKg);

        if (stringMesurment.startsWith("I")) {
            // Imperial
            editTextHeightInches.setVisibility(View.VISIBLE);
            textViewCm.setText("feet and inches");
            textViewKg.setText("pound");

            // Find feet
            try {
                heightCm = Double.parseDouble(stringHeightCm);
            } catch (NumberFormatException nfe) {

            }
            if (heightCm != 0) {
                // Convert CM into feet
                // feet = cm * 0.3937008)/12
                heightFeet = (heightCm * 0.3937008) / 12;
                // heightFeet = Math.round(heightFeet);
                int intHeightFeet = (int) heightFeet;

                editTextHeightCm.setText("" + intHeightFeet);

            }

        } // if(stringMesurment.startsWith("I")){
        else {
            // Metric
            editTextHeightInches.setVisibility(View.GONE);
            textViewCm.setText("cm");
            textViewKg.setText("kg");

            // Change feet and inches to cm

            // Convert Feet
            try {
                heightFeet = Double.parseDouble(stringHeightCm);
            } catch (NumberFormatException nfe) {

            }

            // Convert inches
            try {
                heightInches = Double.parseDouble(stringHeightInches);
            } catch (NumberFormatException nfe) {

            }

            // Need to convert, we want to save the number in cm
            // cm = ((foot * 12) + inches) * 2.54
            if (heightFeet != 0 && heightInches != 0) {
                heightCm = ((heightFeet * 12) + heightInches) * 2.54;
                heightCm = Math.round(heightCm);
                editTextHeightCm.setText("" + heightCm);
            }
        }


        // Weight
        EditText editTextWeight = (EditText) findViewById(R.id.editTextWeight);
        String stringWeight = editTextWeight.getText().toString();
        double doubleWeight = 0;

        try {
            doubleWeight = Double.parseDouble(stringWeight);
        } catch (NumberFormatException nfe) {
        }

        if (doubleWeight != 0) {

            if (stringMesurment.startsWith("I")) {
                // kg to punds
                doubleWeight = Math.round(doubleWeight / 0.45359237);
            } else {
                // pounds to kg
                doubleWeight = Math.round(doubleWeight * 0.45359237);
            }
            editTextWeight.setText("" + doubleWeight);
        }

    } // public voild messuredChanged

    /*- Sign up Submit ---------------------------------------------- */
    public void signUpSubmit() {
        // Error
        TextView textViewErrorMessage = (TextView) findViewById(R.id.textViewErrorMessage);
        String errorMessage = "";

        // Username
        TextInputLayout textInputLayoutName = (TextInputLayout) findViewById(R.id.textInputLayoutName);
        TextInputEditText Name = (TextInputEditText) findViewById(R.id.Name);
        String stringName = Name.getText().toString();
        if (stringName.isEmpty() || stringName.startsWith(" ")) {
            errorMessage = "Please enter a username!";
        }

        String stringUseremail = useremail.getText().toString();


        // Activity level
        Spinner spinnerWeeksPreg = (Spinner) findViewById(R.id.spinnerWeeksPreg);
        int intWeeksPreg = spinnerWeeksPreg.getSelectedItemPosition();



        /* Height */
        EditText editTextHeightCm = (EditText) findViewById(R.id.editTextHeightCm);
        EditText editTextHeightInches = (EditText) findViewById(R.id.editTextHeightInches);
        String stringHeightCm = editTextHeightCm.getText().toString();
        String stringHeightInches = editTextHeightInches.getText().toString();

        double heightCm = 0;
        double heightFeet = 0;
        double heightInches = 0;
        boolean metric = true;

        // Metric or imperial?
        Spinner spinnerMesurment = (Spinner) findViewById(R.id.spinnerMesurment);
        String stringMesurment = spinnerMesurment.getSelectedItem().toString();

        int intMesurment = spinnerMesurment.getSelectedItemPosition();
        if (intMesurment == 0) {
            stringMesurment = "metric";
        } else {
            stringMesurment = "imperial";
            metric = false;
        }

        if (metric == true) {

            // Convert CM
            try {
                heightCm = Double.parseDouble(stringHeightCm);
                heightCm = Math.round(heightCm);
            } catch (NumberFormatException nfe) {
                errorMessage = "Height (cm) has to be a number.";
            }
        } else {

            // Convert Feet
            try {
                heightFeet = Double.parseDouble(stringHeightCm);
            } catch (NumberFormatException nfe) {
                errorMessage = "Height (feet) has to be a number.";
            }

            // Convert inches
            try {
                heightInches = Double.parseDouble(stringHeightInches);
            } catch (NumberFormatException nfe) {
                errorMessage = "Height (inches) has to be a number.";
            }

            // Need to convert, we want to save the number in cm
            // cm = ((foot * 12) + inches) * 2.54
            heightCm = ((heightFeet * 12) + heightInches) * 2.54;
            heightCm = Math.round(heightCm);
        }

        // Weight
        EditText editTextWeight = (EditText) findViewById(R.id.editTextWeight);
        String stringWeight = editTextWeight.getText().toString();
        double doubleWeight = 0;
        try {
            doubleWeight = Double.parseDouble(stringWeight);
        } catch (NumberFormatException nfe) {
            errorMessage = "Weight has to be a number.";
        }
        if (metric == true) {
        } else {
            // Imperial
            // Pound to kg
            doubleWeight = Math.round(doubleWeight * 0.45359237);
        }


        // Activity level
        Spinner spinnerActivityLevel = (Spinner) findViewById(R.id.spinnerActivityLevel);
        //  0: Little to no exercise
        // 1: Light exercise (1–3 days per week)
        // 2: Moderate exercise (3–5 days per week)
        // 3: Heavy exercise (6–7 days per week)
        // 4: Very heavy exercise (twice per day, extra heavy workouts)
        int intActivityLevel = spinnerActivityLevel.getSelectedItemPosition();

        // Error handling
        if (errorMessage.isEmpty()) {
            // Put data into database
            textViewErrorMessage.setVisibility(View.GONE);
            UserDao userDao = ((MyApplication) getApplication()).getDaoSession().getUserDao();
            User user = new User();
            user.setId(auth.getUid());
            user.setEmail(stringUseremail);
            user.setName(stringName);
            user.setWeeks(intWeeksPreg);
            user.setMeasurement(stringMesurment);
            user.setHeight(heightCm);
            user.setWeight(doubleWeight);
            user.setActivity(intActivityLevel);
            user.setCreateDate(new Date());
            userDao.insert(user);

            // Move user back to MainActivity
            Intent i = new Intent(RegisterActivity.this, MainActivity.class);
            startActivity(i);
        } else {
            // There is error
            textViewErrorMessage.setText(errorMessage);
            textViewErrorMessage.setVisibility(View.VISIBLE);
        }
    }

} // public class SignUp

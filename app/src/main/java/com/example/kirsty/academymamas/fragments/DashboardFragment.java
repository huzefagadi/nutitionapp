package com.example.kirsty.academymamas.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.kirsty.academymamas.MyApplication;
import com.example.kirsty.academymamas.R;
import com.example.kirsty.academymamas.Utility;
import com.example.kirsty.academymamas.bean.FoodTaken;
import com.example.kirsty.academymamas.entity.FoodDao;
import com.example.kirsty.academymamas.entity.FoodDiary;
import com.example.kirsty.academymamas.entity.FoodDiaryDao;
import com.example.kirsty.academymamas.entity.User;
import com.example.kirsty.academymamas.entity.UserDao;
import com.example.kirsty.academymamas.interfaces.OnFragmentInteractionListener;
import com.github.mikephil.charting.charts.LineChart;
import com.google.firebase.auth.FirebaseAuth;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class DashboardFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    @BindView(R.id.mainLayout)
    LinearLayout mainLayout;

    @BindView(R.id.reqFolic)
    MaterialEditText reqFolicMt;
    @BindView(R.id.comFolic)
    MaterialEditText comFolicMt;

    @BindView(R.id.reqVitC)
    MaterialEditText reqVitCMt;
    @BindView(R.id.comVitC)
    MaterialEditText comVitCMt;

    @BindView(R.id.reqVitB)
    MaterialEditText reqVitBMt;
    @BindView(R.id.comVitB)
    MaterialEditText comVitBMt;


    @BindView(R.id.reqVitD)
    MaterialEditText reqVitDMt;
    @BindView(R.id.comVitD)
    MaterialEditText comVitDMt;


    @BindView(R.id.reqIron)
    MaterialEditText reqIronMt;
    @BindView(R.id.comIron)
    MaterialEditText comIronMt;


    @BindView(R.id.reqZinc)
    MaterialEditText reqZincMt;
    @BindView(R.id.comZinc)
    MaterialEditText comZincMt;


    @BindView(R.id.reqFibre)
    MaterialEditText reqFibreMt;
    @BindView(R.id.comFibre)
    MaterialEditText comFibreMt;


    @BindView(R.id.reqEnergy)
    MaterialEditText reqEnergyMt;
    @BindView(R.id.comenergy)
    MaterialEditText comEnergyMt;

    @BindView(R.id.dateSelected)
    TextView dateSelected;

    @BindView(R.id.energyDetails)
    TextView energyDetails;



    User user;
    FoodDiaryDao foodDiaryDao;
    FoodDao foodDao;

//    @BindView(R.id.chart)
//    LineChart chart;

    public DashboardFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DashboardFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DashboardFragment newInstance(String param1, String param2) {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, view);
        foodDao = ((MyApplication) getActivity().getApplication()).getDaoSession().getFoodDao();
        foodDiaryDao = ((MyApplication) getActivity().getApplication()).getDaoSession().getFoodDiaryDao();
        user = ((MyApplication) getActivity().getApplication()).getDaoSession().getUserDao().load(FirebaseAuth.getInstance().getUid());

        Calendar yesterday = new GregorianCalendar();
        yesterday.set(Calendar.HOUR_OF_DAY, 0);
        yesterday.set(Calendar.MINUTE, 0);
        yesterday.set(Calendar.SECOND, 0);
        yesterday.set(Calendar.MILLISECOND, 0);


        Calendar tomorrow = new GregorianCalendar();
        tomorrow.set(Calendar.HOUR_OF_DAY, 0);
        tomorrow.set(Calendar.MINUTE, 0);
        tomorrow.set(Calendar.SECOND, 0);
        tomorrow.set(Calendar.MILLISECOND, 0);
        tomorrow.add(Calendar.DAY_OF_MONTH, 1);

        dateSelected.setText(yesterday.get(Calendar.DAY_OF_MONTH) + "-" + (yesterday.get(Calendar.MONTH) + 1) + "-" + yesterday.get(Calendar.YEAR));
        loadEntries(yesterday, tomorrow);
        return view;
    }

    private void loadEntries(Calendar yesterday, Calendar tomorrow) {


        double[] requiredNutrients = new double[7];
        Set set = Utility.map.keySet();
        TreeSet<Integer> myTreeSet = new TreeSet<Integer>();
        myTreeSet.addAll(set);
        Iterator value = myTreeSet.iterator();
        List<Integer> weeks = new ArrayList<>();
        while (value.hasNext()) {
            weeks.add((Integer) value.next());
        }

        long currentUserWeek = (yesterday.getTime().getTime() - user.getCreateDate().getTime()) / 7 + user.getWeeks();
        for (int i = 0; i < weeks.size(); i++) {
            if (i == weeks.size() - 1) {
                requiredNutrients = Utility.map.get(weeks.get(i));
            } else if (currentUserWeek <= weeks.get(i)) {
                requiredNutrients = Utility.map.get(weeks.get(i));
                break;
            }
        }


        double[] consumption = new double[]{0, 0, 0, 0, 0, 0, 0};
        double[] energyConsumption = new double[]{0, 0, 0, 0};

        List<FoodDiary> foodDiaryList = foodDiaryDao.queryBuilder()
                .where(FoodDiaryDao.Properties.Date.gt(yesterday.getTime()), FoodDiaryDao.Properties.Date.lt(tomorrow.getTime())).list();

        List<FoodTaken> foodTakenList = new ArrayList<>();

        for (FoodDiary foodDiary : foodDiaryList) {
            FoodTaken foodTaken = new FoodTaken();
            foodTaken.food = foodDao.load(foodDiary.getFoodId());
            foodTaken.foodDiary = foodDiary;
            foodTakenList.add(foodTaken);

            double delta = foodDiary.getServingSize() / 100.0;

            consumption[0] += foodTaken.food.getFolicPerHundred() * delta;
            consumption[1] += foodTaken.food.getVitCPerHundred() * delta;
            consumption[2] += foodTaken.food.getVitBPerHundred() * delta;
            consumption[3] += foodTaken.food.getVitDPerHundred() * delta;
            consumption[4] += foodTaken.food.getIronPerHundred() * delta;
            consumption[5] += foodTaken.food.getZincPerHundred() * delta;
            consumption[6] += foodTaken.food.getFibrePerHundred() * delta;

            energyConsumption[0] += foodTaken.food.getEnergyPerHundred() * delta;
            energyConsumption[1] += foodTaken.food.getFatPerHundred() * delta;
            energyConsumption[2] += foodTaken.food.getCarbsPerHundred() * delta;
            energyConsumption[3] += foodTaken.food.getProteinsPerHundred() * delta;

        }

        reqFolicMt.setText(String.format("%.2f", requiredNutrients[0]));
        comFolicMt.setText(String.format("%.2f", consumption[0]));

        reqVitCMt.setText(String.format("%.2f", requiredNutrients[1]));
        comVitCMt.setText(String.format("%.2f", consumption[1]));

        reqVitBMt.setText(String.format("%.2f", requiredNutrients[2]));
        comVitBMt.setText(String.format("%.2f", consumption[2]));

        reqVitDMt.setText(String.format("%.2f", requiredNutrients[3]));
        comVitDMt.setText(String.format("%.2f", consumption[3]));

        reqIronMt.setText(String.format("%.2f", requiredNutrients[4]));
        comIronMt.setText(String.format("%.2f", consumption[4]));

        reqZincMt.setText(String.format("%.2f", requiredNutrients[5]));
        comZincMt.setText(String.format("%.2f", consumption[5]));

        reqFibreMt.setText(String.format("%.2f", requiredNutrients[5]));
        comFibreMt.setText(String.format("%.2f", consumption[5]));


        double energy = user.getWeight() * 15.5;
        if(currentUserWeek >=27) {
            energy+=300;
        }
        reqEnergyMt.setText(String.format("%.2f",energy));
        comEnergyMt.setText(String.format("%.2f",energyConsumption[0]));


        StringBuilder sb = new StringBuilder();
        sb.append("Energy : ").append(energyConsumption[0]).append("\n");
        sb.append("Fat : ").append(energyConsumption[1]).append("\n");
        sb.append("Carbs : ").append(energyConsumption[2]).append("\n");
        sb.append("Protein : ").append(energyConsumption[3]).append("\n");
        energyDetails.setText(sb.toString());


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentBackPressed(uri);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle(R.string.dashboard_title);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.changeDate)
    void onClick(View v) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        dateSelected.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        Calendar yesterday = new GregorianCalendar();
                        yesterday.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        yesterday.set(Calendar.MONTH, monthOfYear);
                        yesterday.set(Calendar.YEAR, year);
                        yesterday.set(Calendar.HOUR_OF_DAY, 0);
                        yesterday.set(Calendar.MINUTE, 0);
                        yesterday.set(Calendar.SECOND, 0);
                        yesterday.set(Calendar.MILLISECOND, 0);


                        Calendar tomorrow = new GregorianCalendar();
                        yesterday.set(Calendar.DAY_OF_MONTH, dayOfMonth + 1);
                        yesterday.set(Calendar.MONTH, monthOfYear);
                        yesterday.set(Calendar.YEAR, year);
                        tomorrow.set(Calendar.HOUR_OF_DAY, 0);
                        tomorrow.set(Calendar.MINUTE, 0);
                        tomorrow.set(Calendar.SECOND, 0);
                        tomorrow.set(Calendar.MILLISECOND, 0);

                        loadEntries(yesterday, tomorrow);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }


}

package com.example.kirsty.academymamas.interfaces;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;

public interface OnFragmentInteractionListener {

    void pushFragment(Fragment fragment);
    void onFragmentBackPressed(Uri uri);
    void showActivity(Intent intent);
}

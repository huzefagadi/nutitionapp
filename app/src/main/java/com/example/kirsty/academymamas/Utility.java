package com.example.kirsty.academymamas;

import java.util.HashMap;
import java.util.Map;

public class Utility {

    // folic acid,vitamin c ,Vitamin b,vitamin d ,iron,zinc,fiber
    public static double[] week12 = new double[]{300.0, 10, 2.5, 10, 18, 15, 25};
    public static double[] week26 = new double[]{400, 12, 3, 12, 20, 17, 30};
    public static double[] week40 = new double[]{400.0, 15, 3, 11, 18, 15, 20};
    public static double[] week40Abov = new double[]{300.0, 8, 2.5, 8, 16, 12, 20};


    public static Map<Integer,double[]> map = new HashMap<>();
    static {
        map.put(12,week12);
        map.put(26,week26);
        map.put(40,week40);
        map.put(41,week40Abov);
    }
}

package com.example.kirsty.academymamas.fragments;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;

import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kirsty.academymamas.Constants;
import com.example.kirsty.academymamas.MyApplication;
import com.example.kirsty.academymamas.R;
import com.example.kirsty.academymamas.bean.FoodTaken;
import com.example.kirsty.academymamas.entity.FoodDao;
import com.example.kirsty.academymamas.entity.FoodDiary;
import com.example.kirsty.academymamas.entity.FoodDiaryDao;
import com.example.kirsty.academymamas.interfaces.OnFragmentInteractionListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FoodDiaryFragment extends Fragment {
    /*-  Class Variables -------------------------------------------------------------- */
    private View mainView;
    private Cursor listCursor;


    // Action buttons on toolbar
    private MenuItem menuItemAddFood;
    private FoodDiaryDao foodDiaryDao;
    private FoodDao foodDao;

    // Holding variables
    private String currentDateYear = "";
    private String currentDateMonth = "";
    private String currentDateDay = "";

    private String currentFoodId;
    private String currentFoodName;
    private String currentFdId;

    private boolean lockPortionSizeByPcs;
    private boolean lockPortionSizeByGram;

    private OnFragmentInteractionListener mListener;

    @BindView(R.id.textViewEnergyBreakfast)
    TextView textViewEnergyBreakfast;
    @BindView(R.id.textViewEnergyMorningSnack)
    TextView textViewEnergyMorningSnack;
    @BindView(R.id.textViewEnergyLunch)
    TextView textViewEnergyLunch;
    @BindView(R.id.textViewEnergyAfternoonSnack)
    TextView textViewEnergyAfternoonSnack;
    @BindView(R.id.textViewEnergyDinner)
    TextView textViewEnergyDinner;
    @BindView(R.id.textViewEnergyEveningSnack)
    TextView textViewEnergyEveningSnack;
    @BindView(R.id.breakfastDescription)
    TextView breakfastDescription;

    //Constructor
    public FoodDiaryFragment() {
        // Required empty public constructor
    }

    //Creating Fragment
    public static FoodDiaryFragment newInstance(String param1, String param2) {
        FoodDiaryFragment fragment = new FoodDiaryFragment();
        return fragment;
    }


    //Runs method when started and sets toolbar
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /* Set title */
        getActivity().setTitle(R.string.food_title);

        // getDataFromDbAndDisplay
        initialiseFoodDiary();

        // Create menu
        setHasOptionsMenu(true);
    } // onActivityCreated

    /*- 06 On create view ---------------------------------------------------------------- */
    // Sets main View variable to the view, so we can change views in fragment
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_food_diary, container, false);
        ButterKnife.bind(this, mainView);
        foodDiaryDao = ((MyApplication) getActivity().getApplication()).getDaoSession().getFoodDiaryDao();
        foodDao = ((MyApplication) getActivity().getApplication()).getDaoSession().getFoodDao();
        return mainView;
    }


    /*- 07 set main view ----------------------------------------------------------------- */
    // Changing view method in fragmetn
    private void setMainView(int id) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mainView = inflater.inflate(id, null);
        ViewGroup rootView = (ViewGroup) getView();
        rootView.removeAllViews();
        rootView.addView(mainView);
    }

    /*- Initalize home ------------------------------------------------------------ */
    private void initialiseFoodDiary() {
        /* Find date */

// today
        Calendar yesterday = new GregorianCalendar();
        yesterday.set(Calendar.HOUR_OF_DAY, 0);
        yesterday.set(Calendar.MINUTE, 0);
        yesterday.set(Calendar.SECOND, 0);
        yesterday.set(Calendar.MILLISECOND, 0);


        Calendar tomorrow = new GregorianCalendar();
        tomorrow.set(Calendar.HOUR_OF_DAY, 0);
        tomorrow.set(Calendar.MINUTE, 0);
        tomorrow.set(Calendar.SECOND, 0);
        tomorrow.set(Calendar.MILLISECOND, 0);
        tomorrow.add(Calendar.DAY_OF_MONTH, 1);


        /* Fill table */
        updateTableItems(yesterday, tomorrow);

        /* Calcualte number of calories today */
        //calcualteNumberOfCalEatenToday(stringFdDate);



        /* Breakfast listener */
        ImageView imageViewAddBreakfast = (ImageView) getActivity().findViewById(R.id.imageViewAddBreakfast);
        imageViewAddBreakfast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFood(0); // 0 == Breakfast
            }
        });

        ImageView imageViewAddMorningSnack = (ImageView) getActivity().findViewById(R.id.imageViewAddMorningSnack);
        imageViewAddMorningSnack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFood(1); // 1 == Morning Snack
            }
        });
        ImageView imageViewAddLunch = (ImageView) getActivity().findViewById(R.id.imageViewAddLunch);
        imageViewAddLunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFood(2); // 2 == Lunch
            }
        });

        ImageView imageViewAddAfternoonSnack = (ImageView) getActivity().findViewById(R.id.imageViewAddAfternoonSnack);
        imageViewAddAfternoonSnack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFood(3); // 3 == Afternoon Snack
            }
        });
        ImageView imageViewAddDinner = (ImageView) getActivity().findViewById(R.id.imageViewAddDinner);
        imageViewAddDinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFood(4); // 4 == Dinner
            }
        });
        ImageView imageViewAddEveningSnack = (ImageView) getActivity().findViewById(R.id.imageViewAddEveningSnack);
        imageViewAddEveningSnack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFood(5); // 5 = Snacks
            }
        });


    } // initalizeHome

    /*- Update table ------------------------------------------------------------ */
    private void updateTableItems(Calendar yesterday, Calendar tomorrow) {
        List<FoodDiary> foodDiaryList = foodDiaryDao.queryBuilder()
                .where(FoodDiaryDao.Properties.Date.gt(yesterday.getTime()), FoodDiaryDao.Properties.Date.lt(tomorrow.getTime())).list();

        List<FoodTaken> foodTakenList = new ArrayList<>();
        Map<Integer, Double> mealCountMap = new HashMap<>();
        for (int i = 0; i < 6; i++) {
            mealCountMap.put(i, 0.0);
        }

        Map<Integer, List<String>> mealEntryMap = new HashMap<>();
        for (int i = 0; i < 6; i++) {
            mealEntryMap.put(i, new ArrayList<>());
        }

        for (FoodDiary foodDiary : foodDiaryList) {
            FoodTaken foodTaken = new FoodTaken();
            foodTaken.food = foodDao.load(foodDiary.getFoodId());
            foodTaken.foodDiary = foodDiary;
            foodTakenList.add(foodTaken);
            double newVal = mealCountMap.get(foodDiary.getMealNumber()) + (foodTaken.food.getEnergyPerHundred() / 100 * foodDiary.getServingSize());
            mealCountMap.put(foodDiary.getMealNumber(), newVal);
            mealEntryMap.get(foodDiary.getMealNumber()).add(foodTaken.food.getName());
        }

        for (int key : mealCountMap.keySet()) {
            switch (key) {
                case 0:
                    textViewEnergyBreakfast.setText(String.valueOf(mealCountMap.get(key)));
                    setName(mealEntryMap.get(key), breakfastDescription);
                    break;
                case 1:
                    textViewEnergyMorningSnack.setText(String.valueOf(mealCountMap.get(key)));
                    break;
                case 2:
                    textViewEnergyLunch.setText(String.valueOf(mealCountMap.get(key)));
                    break;
                case 3:
                    textViewEnergyAfternoonSnack.setText(String.valueOf(mealCountMap.get(key)));
                    break;
                case 4:
                    textViewEnergyDinner.setText(String.valueOf(mealCountMap.get(key)));
                    break;
                case 5:
                    textViewEnergyEveningSnack.setText(String.valueOf(mealCountMap.get(key)));
                    break;

            }
        }

    }

    private void setName(List<String> list, TextView textView) {
        StringBuilder sb = new StringBuilder();
        for (String name : list) {
            sb.append(name).append("\n");
        }
        textView.setText(sb.toString());
        if(list.isEmpty()) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
        }
    }

    /*- Add food ------------------------------------------------------------ */
    private void addFood(int mealNumber) {


        FoodFragment fragment = FoodFragment.newInstance();
        // Send variable
        Bundle bundle = new Bundle();
        bundle.putString(Constants.INTENT_MEAL_NUMBER, String.valueOf(mealNumber)); // Put anything what you want
        bundle.putString("currentFoodId", ""); // Put anything what you want
        bundle.putString(Constants.INTENT_ACTION, Constants.ADD_MEAL_FOR_DAY); // Put anything what you want
        fragment.setArguments(bundle);

        mListener.pushFragment(fragment);


    } // initalizeHome

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}


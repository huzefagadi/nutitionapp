package com.example.kirsty.academymamas.fragments;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.SearchView;
import android.widget.TextView;

import com.example.kirsty.academymamas.AddNewFoodActivity;
import com.example.kirsty.academymamas.Constants;
import com.example.kirsty.academymamas.ShowFoodDetailsActivity;
import com.example.kirsty.academymamas.entity.Food;
import com.example.kirsty.academymamas.FragmentActivity;
import com.example.kirsty.academymamas.MyApplication;
import com.example.kirsty.academymamas.R;
import com.example.kirsty.academymamas.adapters.FoodRecyclerViewAdapter;
import com.example.kirsty.academymamas.entity.FoodDao;
import com.example.kirsty.academymamas.interfaces.AdapterCallback;
import com.example.kirsty.academymamas.interfaces.OnFragmentInteractionListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FoodFragment extends Fragment implements AdapterCallback {

    FoodDao mFoodDao;
    @BindView(R.id.listViewFood)
    RecyclerView mRecyclerView;
    /*- 01 Class Variables -------------------------------------------------------------- */
    private View mainView;
    private Cursor listCursor;

    // Action buttons on toolbar
    private MenuItem menuItemEdit;
    private MenuItem menuItemDelete;

    // Holder
    private String currentId = "";
    private String currentName;

    private OnFragmentInteractionListener mListener;

    private SearchView mSearchView;
    private String currentSearchedString;

    private String mealNumber;
    private String mAction;

    /*- 03 Constructur ------------------------------------------------------------------ */
    // Nessesary for having Fragment as class
    public FoodFragment() {
        // Required empty public constructor
    }


    /*- 04 Creating Fragment ------------------------------------------------------------- */
    public static FoodFragment newInstance() {
        FoodFragment fragment = new FoodFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    /*- 05 on Activity Created ---------------------------------------------------------- */
    // Run methods when started
    // Set toolbar menu items
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getActivity().setTitle(R.string.food_title);


        // Create menu
        setHasOptionsMenu(true);

        /* Get data from fragment */
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            currentId = bundle.getString("currentFoodId");

            // Need to run to get edit and delete buttons: onCreateOptionsMenu();
        }

    } // onActivityCreated


    /*- 06 On create view ---------------------------------------------------------------- */
    // Sets main View variable to the view, so we can change views in fragment
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_food, container, false);
        ButterKnife.bind(this, mainView);
        mSearchView = mainView.findViewById(R.id.searchbar);
        mFoodDao = ((MyApplication) getActivity().getApplication()).getDaoSession().getFoodDao();
        initializeSearchView();
        return mainView;
    }

    private void initializeSearchView() {
        populateSearch();
        mSearchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionClick(int position) {
                mSearchView.getSuggestionsAdapter().getCursor().moveToPosition(position);
                String data = mSearchView.getSuggestionsAdapter().getCursor().getString(1);
                mSearchView.setQuery(data, true);
                currentSearchedString = data;
                return true;
            }

            @Override
            public boolean onSuggestionSelect(int position) {
                // Your code here
                return true;
            }
        });

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                currentSearchedString = query;
                //  performSearch(query);
                mSearchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() >= 1) {
                    populateSearchSuggestion(newText);
                }
                return false;
            }
        });
    }

    private void populateSearchSuggestion(String query) {

        final MatrixCursor c = new MatrixCursor(new String[]{BaseColumns._ID, "Name"});
        int count = 0;

        List<Food> foodList = mFoodDao.queryBuilder()
                .where(FoodDao.Properties.Name.like("%" + query + "%")).list();

        for (Food food : foodList) {
            c.addRow(new Object[]{count++, food.getName()});
        }

        if (mSearchView.getSuggestionsAdapter() != null) {
            mSearchView.getSuggestionsAdapter().changeCursor(c);
        }
    }

    public void populateSearch() {
        final String[] from = new String[]{"name"};
        final int[] to = new int[]{android.R.id.text1};
        List<Food> foodList = mFoodDao.loadAll();
        final MatrixCursor c = new MatrixCursor(new String[]{BaseColumns._ID, "Name"});
        int count = 0;

        for (Food food : foodList) {
            c.addRow(new Object[]{count++, food.getName()});
        }
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(getContext(),
                android.R.layout.simple_list_item_1,
                c,
                from,
                to,
                android.support.v4.widget.CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        mSearchView.setSuggestionsAdapter(adapter);
    }

    /*- 07 set main view ----------------------------------------------------------------- */
    // Changing view method in fragmetn
    private void setMainView(int id) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mainView = inflater.inflate(id, null);
        ViewGroup rootView = (ViewGroup) getView();
        rootView.removeAllViews();
        rootView.addView(mainView);
    }


    /*- 08 on Create Options Menu -------------------------------------------------------- */
    // Creating action icon on toolbar
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        // Inflate menu
        ((FragmentActivity) getActivity()).getMenuInflater().inflate(R.menu.menu_food, menu);

        // Assign menu items to variables
        menuItemEdit = menu.findItem(R.id.menu_action_food_edit);
        menuItemDelete = menu.findItem(R.id.menu_action_food_delete);

        // Hide as default
        menuItemEdit.setVisible(false);
        menuItemDelete.setVisible(false);
    }

    /*- 09 on Options Item Selected ------------------------------------------------------ */
    // Action icon clicked on
    // Menu
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        int id = menuItem.getItemId();
        if (id == R.id.menu_action_food_add) {
            addFood();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onResume() {
        super.onResume();
        populateListFood();
    }

    /*- populate List -------------------------------------------------------------- */
    public void populateListFood() {
        List<Food> foodList = mFoodDao.loadAll();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(new FoodRecyclerViewAdapter(getContext(), foodList, this));
    }



    /*- List item clicked ------------------------------------------------------------ */
    public void listItemClicked(long foodItem) {
        Intent intent = new Intent(getActivity(), ShowFoodDetailsActivity.class);
        intent.putExtra(Constants.INTENT_FOOD_ID, foodItem);
        if (getArguments() != null && getArguments().get(Constants.INTENT_ACTION) != null) {
            intent.putExtra(Constants.INTENT_ACTION, (String) getArguments().get(Constants.INTENT_ACTION));
            intent.putExtra(Constants.INTENT_MEAL_NUMBER, (String) getArguments().get(Constants.INTENT_MEAL_NUMBER));
        }
        mListener.showActivity(intent);
    }

    /*- Add food ------------------------------------------------------------------------------ */
    public void addFood() {
        Intent intent = new Intent(getActivity(), AddNewFoodActivity.class);
        mListener.showActivity(intent);
    } // addFood


    /*- addFoodToDiarySelectMealNumber ------------------------------------------------------ */
    public void addFoodToDiarySelectMealNumber() {
        /* Change layout */
        int newViewID = R.layout.fragment_home_select_meal_number;
        setMainView(newViewID);


        TextView textViewBreakfast = (TextView) getActivity().findViewById(R.id.textViewBreakfast);
        textViewBreakfast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFoodToDiarySelectedMealNumberMoveToAdd(0);
            }
        });
        TextView textViewMorningSnack = (TextView) getActivity().findViewById(R.id.textViewMorningSnack);
        textViewMorningSnack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFoodToDiarySelectedMealNumberMoveToAdd(1);
            }
        });

        TextView textViewLunch = (TextView) getActivity().findViewById(R.id.textViewLunch);
        textViewLunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFoodToDiarySelectedMealNumberMoveToAdd(2);
            }
        });


        TextView textViewAfternoonSnack = (TextView) getActivity().findViewById(R.id.textViewAfternoonSnack);
        textViewAfternoonSnack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFoodToDiarySelectedMealNumberMoveToAdd(3);
            }
        });

        TextView textViewDinner = (TextView) getActivity().findViewById(R.id.textViewDinner);
        textViewDinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFoodToDiarySelectedMealNumberMoveToAdd(4);
            }
        });

        TextView textViewEveningSnack = (TextView) getActivity().findViewById(R.id.textViewEveningSnack);
        textViewEveningSnack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFoodToDiarySelectedMealNumberMoveToAdd(5);
            }
        });


    } // addFoodToDiarySelectMealNumber

    /*- addFoodToDiarySelectedMealNumberMoveToAdd -------------------------------------------- */
    public void addFoodToDiarySelectedMealNumberMoveToAdd(int mealNumber) {

        /* Inialize fragmet */
        Fragment fragment = null;
        Class fragmentClass = null;
        fragmentClass = FoodDiaryFragment.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Send variable
        Bundle bundle = new Bundle();
        bundle.putString("mealNumber", "" + mealNumber); // Put anything what you want
        bundle.putString("currentFoodId", "" + mealNumber); // Put anything what you want
        bundle.putString("action", "foodInCategoryListItemClicked"); // Put anything what you want
        fragment.setArguments(bundle);

        // Need to pass meal number
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mealNumber = getArguments().getString(Constants.INTENT_MEAL_NUMBER, null);
            mAction = getArguments().getString(Constants.ADD_MEAL_FOR_DAY, null);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClicked(int position) {
        listItemClicked(((FoodRecyclerViewAdapter) mRecyclerView.getAdapter()).getData().get(position).getId());
    }
}